package com.example.usersapispring.repo;

import com.example.usersapispring.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IUsersRepo extends JpaRepository<User,String> {
       // User findByUserId(String id);
        User findByUsername(String username);
}
