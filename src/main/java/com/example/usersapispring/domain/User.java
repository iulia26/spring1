package com.example.usersapispring.domain;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Data
@Table(name = "Users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {
    @Id
    @Column(name = "user_id", nullable = false,unique = true)
    private String userId;
    @Column(name = "first_name")
    private String firstName;
    @Column (name  = "last_name")
    private String lastName;
    @Column (name = "email")
    private String email;
    @Column (name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name  = "phone")
    private String phone;

    //List<Holiday> holidays;
    //Daca as avea aici un al tip de data ,ex MyClassEx instance ,cum s-ar mapa la baza de date?

    @Override
    public String toString() {
        return "User{" +
                "id='" + userId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
