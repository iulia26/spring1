package com.example.usersapispring.controller;


import com.example.usersapispring.domain.User;
import com.example.usersapispring.networking.CustomMessage;
import com.example.usersapispring.networking.Response;
import com.example.usersapispring.service.IUserService;
import com.example.usersapispring.service.UserService;
import lombok.RequiredArgsConstructor;
//import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.time.LocalDateTime.now;
import static java.util.Map.of;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final IUserService userService;
    private final MessageController messageController;

    @GetMapping("/list")
    public ResponseEntity<Response> getUsers(@RequestParam(value = "limit") Optional<Integer> pageLimit, @RequestParam(value = "page") Optional<Integer> currentPage) {
        messageController.publishMessage(new CustomMessage(null,"Get all users request",null));
        String message = "";
        if (pageLimit.isEmpty()) message += "No query param to specify page limit,default value = 1\n";
        if (currentPage.isEmpty()) message += "No query param to specify the page number,default value  = 1\n";
        if (message.equals("")) message = "Retrieved users sucessfully";
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .data(of("users", userService.getAllUsers(currentPage.orElse(0), pageLimit.orElse(1))))
                        .status(HttpStatus.OK)
                        .statusCode(HttpStatus.OK.value())
                        .developerMessage(message)
                        .build()

        );

    }

    @GetMapping("/{userid}")
    public ResponseEntity<?> getUserById(@PathVariable("userid") Optional<String> userId) {
        String message = "User found";
        try {
            User result = userService.findById(userId.orElse("null"));
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            ResponseEntity<String> body = ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
            body.getHeaders().add("ana","bana");
            return body;
        }
    }

    @PostMapping("/save")
    public ResponseEntity<Response> saveUser(@RequestBody User user) {
        userService.addUser(user);

        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(now())
                        .message("User added")
                        .status(CREATED)
                        .statusCode(CREATED.value())
                        .build()
        );

    }

    @PutMapping("/update")
    public ResponseEntity<Response> updateUser(@RequestBody User user) throws Exception {
        try {

            userService.updateUser(user);
            Response response = Response.builder()
                    .timeStamp(now())
                    .message("User updated")
                    .data(of("Updated user", userService.findById(user.getUserId())))
                    .status(HttpStatus.OK)
                    .statusCode(HttpStatus.OK.value())
                    .build();

            return ResponseEntity.ok(

                    response
            );
        } catch (IllegalArgumentException ex) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Response.builder()
                            .timeStamp(now())
                            .message(ex.getMessage())
                            .statusCode(HttpStatus.BAD_REQUEST.value())
                            .build()
            );

        }

    }

    @DeleteMapping("/delete/{username}")
    public ResponseEntity<Response> deleteUser(@PathVariable("username") String username) {
        Optional<User> user = userService.findByUsername(username);
        if (user.isPresent()) {
            userService.deleteUser(user.get());
            return ResponseEntity.ok(
                    Response.builder()
                            .timeStamp(now())
                            .message("Deleted user")
                            .status(HttpStatus.OK)
                            .build()
            );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                Response.builder().timeStamp(now()).message("User not found").build()
        );
    }

}
