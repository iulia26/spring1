package com.example.usersapispring.controller;


import com.example.usersapispring.networking.CustomMessage;
import com.example.usersapispring.rabbitmq.MqConfig;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Controller
public class MessageController {

    @Autowired
    private RabbitTemplate template;

    public void publishMessage(CustomMessage message){

        message.setId(UUID.randomUUID().getMostSignificantBits());
        message.setTimestamp(new Date());
        template.convertAndSend(MqConfig.EXCHANGE,MqConfig.ROUTING_KEY
        ,message);


    }

}
