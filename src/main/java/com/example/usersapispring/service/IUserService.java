package com.example.usersapispring.service;

import com.example.usersapispring.domain.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    List<User> getAllUsers(int page,int limit);
    User findById(String userId) throws Exception;
    Optional<User> findByUsername(String username);
    void addUser(User user);
    void updateUser(User user);
    void deleteUser(User user);
}
