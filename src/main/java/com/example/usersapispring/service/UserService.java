package com.example.usersapispring.service;

import com.example.usersapispring.domain.User;
import com.example.usersapispring.repo.IUsersRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transaction;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements  IUserService{
        private final IUsersRepo usersRepo;

        @Override
        public List<User> getAllUsers(int page,int limit) {
                return usersRepo.findAll(PageRequest.of(page,limit)).toList();
        }

        @Override
        public User findById(String userId) throws Exception {

                return usersRepo.findById(userId).orElseThrow(() -> new Exception("Error find by id"));
        }

        @Override
        public Optional<User> findByUsername(String username) {
                return Optional.ofNullable(usersRepo.findByUsername(username));
        }

        @Override
        public void addUser(User user) {
                usersRepo.save(user);
        }

        @Override
        public void updateUser(User user) {
                Optional<User> userByUsername = Optional.ofNullable(usersRepo.findByUsername(user.getUsername()));

                if(userByUsername.isEmpty())
                        throw new IllegalArgumentException("This user doesen't exist,user id:"+user.getUserId());
                user.setUserId(userByUsername.get().getUserId());
                try {
                        usersRepo.deleteById(user.getUserId());
                        usersRepo.save(user);
                }
                catch (IllegalArgumentException ex){
                                log.error(ex.getMessage());
                                throw  new IllegalArgumentException("Update error");
                }

        }

        @Override
        public void deleteUser(User user) {
                try{
                        usersRepo.deleteById(user.getUserId());
                }catch (IllegalArgumentException ex){
                        log.error(ex.getMessage());
                        throw  new IllegalArgumentException("Delete error");
                }
        }

}
