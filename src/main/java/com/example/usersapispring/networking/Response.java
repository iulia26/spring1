package com.example.usersapispring.networking;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Builder
//@JsonInclude annotation provides Include.NON_NULL attribute to ignore fields with Null values and
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    private LocalDateTime timeStamp;
    private int statusCode;
    private HttpStatus status;
    protected String reason;
    protected String message;
    protected String developerMessage;
    protected Map<?,?> data;
    


}
